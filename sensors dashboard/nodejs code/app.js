const port = 'COM3'
    , speed = 2000000;

var app = require('express')()
    , server = require('http').createServer(app)
    , io = require('socket.io').listen(server)
    , serialport = require('serialport')
    , serial1 = new serialport(port, { baudRate: speed, parser: new serialport.parsers.Readline('\r\n') }, err => {
        if (err) {
            console.error('Arduino is not connnected on ' + port);
            process.exit(0);
        }
    });

app.get('/', function (req, res) { res.sendfile(__dirname + '/index.html'); });
app.get('/style.css', function (req, res) { res.sendfile(__dirname + '/style.css'); });

io.sockets.on('connection', function (socket) {
    serial1.on('data', data => socket.emit('message', data.toString('utf8')));
    socket.on('led', message => { serial1.write(message + '\n'); });
    socket.on('lcd', message => { serial1.write(message + '\n'); });
});

server.listen(8080);