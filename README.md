# Dépôt Arduino
Author      Tom De Rudder  
Version     0.0.3
___
> "_Arduino_" regroupe tous mes programmes pour cartes Arduino.

## Architecture
```bash
	Arduino
	├── char editor
    │   ├── arduino code/ # example de code généré
    │   ├── nodejs code/ # serveur nodejs qui rend le site
    │   └── render/ # aperçu du projet (images, vidéos)
    │
	├── lib
    │   ├── Encoder.zip # Librairie pour l'utilisation d'un codeur
    │   └── LcdMenu.zip # Librairie pour crée un menu déroulant avec un LCD et un codeur
    │
    ├── sensors dashboard
    │   ├── arduino code/ # code de l'arduino qui communique avec le pc au travers du port série
    │   ├── nodejs code/ # serveur nodejs qui communique avec l'arduino et le site
    │   └── render/ # aperçu du projet (images, vidéos)
    │
	└── README.md
```

## Pré requis
- arduino
- IDE pour arduino
- matériel électronique (breadboard, led, codeur, etc)

## Installation
```bash
    git clone "https://gitlab.com/tomderudderetna/Arduino.git" arduino
    cd ./arduino
```

## Liens utiles
### Résistance
 - [Code Couleur des Résistances](https://www.dcode.fr/code-couleur-resistance)

### Joystick
 - [Utiliser un joystick 2 axes](https://letmeknow.fr/blog/2013/11/07/tuto-utiliser-un-joystick-2-axes/)
 - [How to connect and use an Analog Joystick with an Arduino - Tutorial](https://www.youtube.com/watch?v=MlDi0vO9Evg)

### Codeur
 - [Arduino – Rotary encoder (Keyes KY-040)](http://domoticx.com/arduino-rotary-encoder-keyes-ky-040)
 - [Codeur rotatif et Arduino](https://electroniqueamateur.blogspot.com/2014/08/codeur-rotatif-et-arduino.html)

### Afficheur LCD
 - [Programmez un écran LCD](https://openclassrooms.com/fr/courses/3290206-perfectionnez-vous-dans-la-programmation-arduino/3342221-programmez-un-ecran-lcd)  
 - [Create custom characters for the Arduino LCD 1602](https://www.youtube.com/watch?v=jaD0dJtovwc)

### Interruptions
 - [Interruptions](http://www.mon-club-elec.fr/pmwiki_reference_arduino/pmwiki.php?n=Main.AttachInterrupt)

### Série
 - [ARDUINO FUNCTION SERIAL.READ() AND SERIAL.READSTRING()](https://www.instructables.com/id/Arduino-Function-Serialread-And-SerialreadString/)

### Librarie
 - [Writing a Library for Arduino](https://www.arduino.cc/en/Hacking/libraryTutorial)

## Logo
![arduino](https://assets.gitlab-static.net/uploads/-/system/project/avatar/7628262/arduino-1-logo-png-transparent.png)