const port = 8080
    , express = require('express')
    , app = express()

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html')
})
app.get('/style.css', function (req, res) {
    res.sendFile(__dirname + '/style.css')
})
app.get('/script.js', function (req, res) {
    res.sendFile(__dirname + '/script.js')
})

app.listen(port, function () {
    console.log('Example app listening on port ' + port + '!')
})
