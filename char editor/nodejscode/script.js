const dark = '#7f8c8d'
    , ligth = '#fff'
    , charWidth = 5
    , charHeight = 8
    , pixelWidth = 60
    , pixelHeight = 60
    , margin = 1.00
    , code = document.getElementById('code')
    , canvas = document.getElementById('canvas');

let penColor = dark
    , clipboard = ''
    , writable = false;

const matrix = {
    datas: [],
    start: function () {
        for (let x = 0; x < charHeight; x++) {
            this.datas.push([]);
            for (let y = 0; y < charWidth; y++) {
                this.datas[x].push(0);
            }
        }
    },
    update: function () {
        ctx = app.context;
        for (let x = 0; x < this.datas.length; x++) {
            for (let y = 0; y < this.datas[x].length; y++) {
                ctx.fillStyle = this.datas[x][y] ? dark : ligth;
                ctx.fillRect(margin * y * pixelWidth, margin * x * pixelHeight, pixelWidth, pixelHeight);
            }
        }
    }
};

const app = {
    canvas: canvas,
    components: [],
    start: function () {
        this.canvas.width = margin * charWidth * pixelWidth;
        this.canvas.height = margin * charHeight * pixelHeight;
        this.components.push(matrix);
        this.context = this.canvas.getContext("2d");
        this.interval = setInterval(() => this.update(this), 1);
        this.components.forEach(value => value.start())
    },
    clear: function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    update: function () {
        this.clear();
        this.components.forEach(value => value.update())
    }
}

function changePixel(e) {
    const x = parseInt(e.offsetX / pixelWidth / margin)
        , y = parseInt(e.offsetY / pixelHeight / margin);

    if (x >= 0 && x < charWidth && y >= 0 && y < charHeight) {
        if (penColor == dark && !matrix.datas[y][x]) {
            matrix.datas[y][x] = 1;
            setCode();
        }
        else if (penColor == ligth && matrix.datas[y][x]) {
            matrix.datas[y][x] = 0;
            setCode();
        }
    }
};

function setColor(color) {
    if (color == ligth) {
        document.getElementById('colorLigth').classList.remove('btn-stroke');
        document.getElementById('colorDark').classList.add('btn-stroke');
    }
    else {
        document.getElementById('colorLigth').classList.add('btn-stroke');
        document.getElementById('colorDark').classList.remove('btn-stroke');
    }
    penColor = color;
}

function setCode() {
    let str = 'byte myChar[8] = {\n';
    for (let x = 0; x < charHeight; x++) {
        str += "\tB";
        for (let y = 0; y < charWidth; y++)
            str += matrix.datas[x][y];
        str += (1 + x < charHeight) ? ",\n" : "\n";
    }
    code.textContent = str + '};\n\n';
}

function copyToClipboard() {
    document.querySelectorAll('pre').forEach(value => clipboard += value.textContent)
    const tmpTag = document.createElement('textarea');
    document.body.appendChild(tmpTag);
    tmpTag.value = clipboard;
    tmpTag.select();
    document.execCommand('copy');
    document.body.removeChild(tmpTag);
    clipboard = "";
    alert('copy !');
}

canvas.addEventListener('click', e => changePixel(e))
canvas.addEventListener('mousedown', e => writable = true || changePixel(e))
canvas.addEventListener('mouseup', () => writable = false)
canvas.addEventListener('mousemove', e => writable && changePixel(e))
canvas.addEventListener('mouseleave', () => writable = false)

app.start();