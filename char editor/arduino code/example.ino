#include <Wire.h>
#include <LiquidCrystal.h>

enum {RS=13, E=12, D4=11, D5=10, D6=9, D7=8};

LiquidCrystal lcd(RS, E, D4, D5, D6, D7);

byte myChar[8] = {
  B01000,
  B01100,
  B01110,
  B01111,
  B01100,
  B01000,
  B11111,
  B01110
};

void setup() {
    lcd.begin(16,2);
    lcd.createChar (0, myChar);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(char(0));
}

void loop() {}
